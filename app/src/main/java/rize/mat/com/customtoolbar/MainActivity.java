package rize.mat.com.customtoolbar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
private Toolbar toolbars;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbars = (Toolbar)findViewById(R.id.toolbar);
        toolbars.setNavigationIcon(R.mipmap.ic_launcher);
        toolbars.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbars);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.alerts)
        {
            Intent intent = new Intent(getApplicationContext(),SubActivity.class);
            startActivity(intent);

        }
        return true;
    }
}
